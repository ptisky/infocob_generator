<?php
session_start();

/**
 * Permet de supprimer un dossier avec ses fichiers
 * @param $dirname
 * @return bool
 */
function delete_files($dirname)
{
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while ($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname . "/" . $file))
                unlink($dirname . "/" . $file);
            else
                delete_files($dirname . '/' . $file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Wordpress fonctions ***/
require_once('wp-load.php');
require_once('wp-admin/includes/upgrade.php');
require_once('wp-includes/wp-db.php');

/*** Retire les fichiers & plugins inutiles ***/
unlink('./license.txt');
unlink('./readme.html');
unlink('./wp-content/plugins/hello.php');
delete_files('./wp-content/plugins/akismet');

/*** Retire les posts inutiles ***/
if ($_POST['default_content'] == '1') {
    wp_delete_post(1, true); #Hello World
    wp_delete_post(2, true); #Exemple page
}

session_destroy();

echo "true";