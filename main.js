/**
 * Infocob Wordpress Générator
 *
 * Ce projet à pour but le télécharger et l'installation d'un site internet sous Wordpress.
 * Il permet l'installation des plugins et des thèmes liés aux projet récurant de InfocobWeb.
 * Il y a aussi une fonctionnalité jumelle lié à l'extention Infocob configu permetant de
 * récuperer les informations (couleurs du site, police, etc.. ) et les appliquer à ce projet.
 *
 * Toutes utilisations de ce projet est strictement réservé à l'équipe interne de InfocobWeb.
 *
 * @package infocobWP.local
 * @author InfocobWEB
 * @version 1.0
 */

$(document).ready(function () {

    /*** Initialise des variables par default ***/
    let user = "";
    let dbname = "";
    let dbpre = "";
    let steps = [];

    /** Vérifie à quelle étape on se situe **/
    let validation = {
        init: false,
        step0: false,
        step1: false,
        step2: false,
        step3: false,
        step4: false,
        step5: false,
        step6: false,
        step7: false,
        step8: false
    };

    /**
     * Récupère l'étape actuelle et attend que la précedente soit terminé avant de lancer la suivante
     * @param step
     */
    function generate(step) {

        switch (step) {
            case 0:
                getSteps("init.php", validation.init, "0", "<p>Initialisation du script ..... ok</p>", 1);
                break;
            case 1:
                getSteps("step0_tests.php", validation.step0, "0", "<p>Réalisation de tests essentiels ..... ok</p>", 2);
                break;
            case 2:
                getSteps("step1_downloadWP.php", validation.step1, "1", "<p>Téléchargement de Wordpress & Unzip ..... ok</p>", 3);
                break;
            case 3:
                getSteps("step2_generatewpconfig.php", validation.step2, "2", "<p>Configuration du wp-config ..... ok</p>", 4);
                break;
            case 4:
                getSteps("step3_installWP.php", "3", validation.step3, "<p>Instalation de Wordpress ..... ok</p>", 5);
                break;
            case 5:
                getSteps("step4_initDefaultValues.php", validation.step4, "4", "<p>Paramétrage de Wordpress ..... ok</p>", 6);
                break;
            case 6:
                getSteps("step5_installExtentions.php", validation.step5, "5", "<p>Téléchargement & activation des extentions ..... ok</p>", 7);
                break;
            case 7:
                getSteps("step6_installThemes.php", validation.step6, "6", "<p>Téléchargement & installation des thèmes ..... ok</p>", 8);
                break;
            case 8:
                getSteps("step7_createPages.php", validation.step7, "7", "<p>Création des pages par default ..... ok</p>", 9);
                break;
            case 9:
                getSteps("step8_clean.php", validation.step8, "8", "<p>Nettoyage des élèments inutiles ..... ok</p>", 666);
                break;
        }
    }

    /**
     * Sélectionne la bonne étape dans le loader affiché
     * @param stepNum
     */
    function progress(stepNum) {
        steps.forEach((e) => {
            if (e.id === stepNum) {
                e.classList.add('selected');
                e.classList.remove('completed');
            }
            if (e.id < stepNum) {
                e.classList.add('completed');
            }
            if (e.id > stepNum) {
                e.classList.remove('selected', 'completed');
            }
        });
    }

    /**
     * Récupère l'étape d'instalation en cours et redirige vers le script php associé
     * @param thisState
     * @param validation
     * @param step
     * @param messageLOG
     * @param nextStep
     * @return {*}
     */
    function getSteps(thisState, validation, step, messageLOG, nextStep) {
        let Datas = {
            host: $('#host').val(),
            dbname: $('#dbname').val(),
            dbuser: $('#dbuser').val(),
            dbpdw: $('#dbpdw').val(),
            url: $('#url').val(),
            sitename: $('#sitename').val(),
            mailuser: $('#mailuser').val(),
            siteuser: $('#siteuser').val(),
            pdwuser: $('#pdwuser').val(),
            dbprefix: $('#dbprefix').val(),
            demoSelector: $('input[name=demoSelector]:checked').val()

        };

        return $.ajax({
            type: 'GET',
            url: "install/" + thisState,
            data: Datas,
            //async: false,
            //dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                progress(step);
                $(".logInstallContainer").append(messageLOG);
                validation = true;

                if (nextStep !== 666) {
                    generate(nextStep);
                } else {
                    let site = convertToSlug($('#sitename').val());
                    $(".logInstallContainer").append("<p class='sucess_install'>Instalation effectué !</p>");
                    $(".logInstallContainer").append("<p class='sucess_install'>user : " + $('#siteuser').val() + "</p>");
                    $(".logInstallContainer").append("<p class='sucess_install'>Mot de passe : " + $('#pdwuser').val() + "</p>");
                    window.open('http://localhost/' + site + '.local', '_blank');
                    window.open('http://localhost/' + site + '.local/wp-admin', '_blank');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.responseJSON.message) {
                    $(".logInstallContainer").append("<p class='error'>Erreur : " + XMLHttpRequest.responseJSON.message + "</p>");
                } else {
                    $(".logInstallContainer").append("<p class='error'>Erreur : " + textStatus + "</p>");
                }

            }

        });
    }

    /**
     * Permet de drag & drop les fichiers
     */
    function ekUpload() {
        function Init() {


            var fileSelect = document.getElementById('file-upload'),
                fileDrag = document.getElementById('file-drag'),
                submitButton = document.getElementById('submit-button');

            fileSelect.addEventListener('change', fileSelectHandler, false);

            // Is XHR2 available?
            var xhr = new XMLHttpRequest();
            if (xhr.upload) {
                // File Drop
                fileDrag.addEventListener('dragover', fileDragHover, false);
                fileDrag.addEventListener('dragleave', fileDragHover, false);
                fileDrag.addEventListener('drop', fileSelectHandler, false);
            }
        }

        function fileDragHover(e) {
            var fileDrag = document.getElementById('file-drag');

            e.stopPropagation();
            e.preventDefault();

            fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
        }

        function fileSelectHandler(e) {
            // Fetch FileList object
            var files = e.target.files || e.dataTransfer.files;

            // Cancel event and hover styling
            fileDragHover(e);

            // Process all File objects
            for (var i = 0, f; f = files[i]; i++) {
                parseFile(f);
                uploadFile(f);
            }
        }

        // Output
        function output(msg) {
            // Response
            var m = document.getElementById('messages');
            m.innerHTML = msg;
        }

        function parseFile(file) {

            console.log(file.name);
            output(
                '<strong>' + encodeURI(file.name) + '</strong>'
            );

            // var fileType = file.type;
            // console.log(fileType);
            var imageName = file.name;

            var isGood = (/\.(?=json)/gi).test(imageName);
            if (isGood) {
                document.getElementById('start').classList.add("hidden");
                document.getElementById('response').classList.remove("hidden");
                document.getElementById('notimage').classList.add("hidden");
                // Thumbnail Preview
                document.getElementById('file-image').classList.remove("hidden");
                //document.getElementById('file-image').src = URL.createObjectURL(file);
            } else {
                document.getElementById('file-image').classList.add("hidden");
                document.getElementById('notimage').classList.remove("hidden");
                document.getElementById('start').classList.remove("hidden");
                document.getElementById('response').classList.add("hidden");
                document.getElementById("file-upload-form").reset();
            }
        }

        function setProgressMaxValue(e) {
            var pBar = document.getElementById('file-progress');

            if (e.lengthComputable) {
                pBar.max = e.total;
            }
        }

        function updateFileProgress(e) {
            var pBar = document.getElementById('file-progress');

            if (e.lengthComputable) {
                pBar.value = e.loaded;
            }
        }

        function uploadFile(file) {

            var xhr = new XMLHttpRequest(),
                fileInput = document.getElementById('class-roster-file'),
                pBar = document.getElementById('file-progress'),
                fileSizeLimit = 1024; // In MB
            if (xhr.upload) {
                // Check if file is less than x MB
                if (file.size <= fileSizeLimit * 1024 * 1024) {
                    // Progress bar
                    pBar.style.display = 'inline';
                    xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
                    xhr.upload.addEventListener('progress', updateFileProgress, false);

                    // File received / failed
                    xhr.onreadystatechange = function (e) {
                        if (xhr.readyState == 4) {
                            // Everything is good!

                            // progress.className = (xhr.status == 200 ? "success" : "failure");
                            // document.location.reload(true);
                        }
                    };

                    // Start upload
                    xhr.open('POST', document.getElementById('file-upload-form').action, true);
                    xhr.setRequestHeader('X-File-Name', file.name);
                    xhr.setRequestHeader('X-File-Size', file.size);
                    xhr.setRequestHeader('Content-Type', 'multipart/form-data');
                    xhr.send(file);
                } else {
                    output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
                }
            }
        }

        // Check for the various File API support.
        if (window.File && window.FileList && window.FileReader) {
            Init();
        } else {
            document.getElementById('file-drag').style.display = 'none';
        }
    }

    /**
     * Slugify une chaine de caractère
     * @param Text
     * @return {string}
     */
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/ /g, '')
            .replace(/[^\w-]+/g, '');
    }

    /**
     * Génère un mot de passe PUISSANT
     * @type {{_pattern: RegExp, _getRandomByte: ((function(): (number))|*), generate: (function(*=): string)}}
     */
    let Password = {

        _pattern: /[a-zA-Z0-9_\-\+\.]/,


        _getRandomByte: function () {
            // http://caniuse.com/#feat=getrandomvalues
            if (window.crypto && window.crypto.getRandomValues) {
                let result = new Uint8Array(1);
                window.crypto.getRandomValues(result);
                return result[0];
            } else if (window.msCrypto && window.msCrypto.getRandomValues) {
                let result = new Uint8Array(1);
                window.msCrypto.getRandomValues(result);
                return result[0];
            } else {
                return Math.floor(Math.random() * 256);
            }
        },

        generate: function (length) {
            return Array.apply(null, {'length': length})
                .map(function () {
                    var result;
                    while (true) {
                        result = String.fromCharCode(this._getRandomByte());
                        if (this._pattern.test(result)) {
                            return result;
                        }
                    }
                }, this)
                .join('');
        }

    };

    /**
     * Reset les champs
     */
    function resetInputs() {
        $("#sitename").val("");
        $("#file-upload").val("");
        $("#mailuser").val("");
        $("#pdwuser").val("");
        $("#host").val("");
        $("#dbname").val("");
        $("#dbuser").val("");
        $("#dbpdw").val("");
        $("#dbprefix").val("");
        $("#siteuser").val("");
    }

    /**
     * Insert le dom élément correspondant à l'étape dans un tableau
     */
    Array.prototype.forEach.call($(".step"), (e) => {
        steps.push(e);
    });

    /*** Au click génère un mot de passe ***/
    $(".gen_mdp").click(function () {
        $("#pdwuser").val(Password.generate(16));
    });

    /*** Génère un mot de passe par default ***/
    $("#pdwuser").val(Password.generate(16));

    /*** Remplis automatique après une entrée dans le champs nom du site les autres champs ***/
    $("#sitename").keyup(function () {
        let value = $(this).val();
        let nameMin = convertToSlug(value);

        user = "admin@" + nameMin;
        dbname = "bd_wp_" + nameMin;
        dbpre = nameMin.slice(0, 2) + "_";

        $("#siteuser").val(user);
        $("#dbname").val(dbname);
        $("#dbprefix").val(dbpre);
    });

    /*** Active le drag & drop ***/
    ekUpload();

    /*** Permet de reset les champs ***/
    $(".reset").click(function () {
        resetInputs();
    });

    /*** Remplis les champs avec un environnement de test ***/
    $(".testbuton").click(function () {
        $("#sitename").val("mon site");
        $("#siteuser").val("admin");
        $("#pdwuser").val("123");
        $("#host").val("localhost");
        $("#dbname").val("1234");
        $("#dbuser").val("root");
        $("#dbpdw").val("");
        $("#dbprefix").val("mo_");
        $("#mailuser").val("techniqueweb@infocob-solutions.com");
    });

    /*** Lance la génération de Wordpress ***/
    $(".go_gen").click(function () {
        if ($("#sitename").val() !== "" &&
            $("#siteuser").val() !== "" &&
            $("#pdwuser").val() !== "" &&
            $("#host").val() !== "" &&
            $("#dbname").val() !== "" &&
            $("#dbuser").val() !== "" &&
            $("#dbprefix").val() !== "") {

            $('.card_loader').show();
            $('.right-container').addClass("load");

            generate(0);
        } else {
            console.log("Les champs ne sont pas tous remplis");
        }
    });

});