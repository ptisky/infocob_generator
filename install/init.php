<?php

session_start();

@set_time_limit(0);

/**** Base de donnée ****/
$_SESSION['host'] = $_GET["host"];
$_SESSION['dbname'] = $_GET["dbname"];
$_SESSION['dbuser'] = $_GET["dbuser"];
$_SESSION['dbpdw'] = $_GET["dbpdw"];

/**** Informations utilisateur ****/
$_SESSION['mailuser'] = $_GET["mailuser"];
$_SESSION['siteuser'] = $_GET["siteuser"];
$_SESSION['pdwuser'] = $_GET["pdwuser"];
$_SESSION['sitename'] = $_GET["sitename"];

/**** Informations thème selectionné ****/
$_SESSION['demoSelector'] = $_GET["demoSelector"];

/**** Le slug du projet ****/
$_SESSION['directory'] = slugify($_GET["sitename"]) . ".local";

/**** Lien du projet ****/
$_SESSION['url'] = "http://localhost/" . $_SESSION['directory'];

/**** Replace wp-config ****/
/*********************************************************************/
$_SESSION['dbname_from'] = "define( 'DB_NAME', 'votre_nom_de_bdd' );";
$_SESSION['dbname_to'] = "define( 'DB_NAME', '" . $_SESSION["dbname"] . "' );";

$_SESSION['dbuser_from'] = "define( 'DB_USER', 'votre_utilisateur_de_bdd' );";
$_SESSION['dbuser_to'] = "define( 'DB_USER', '" . $_SESSION["dbuser"] . "' );";

$_SESSION['dbpass_from'] = "define( 'DB_PASSWORD', 'votre_mdp_de_bdd' );";
$_SESSION['dbpass_to'] = "define( 'DB_PASSWORD', '" . $_SESSION["dbpdw"] . "' );";

$_SESSION['dbhost_from'] = "define( 'DB_HOST', 'localhost' );";
$_SESSION['dbhost_to'] = "define( 'DB_HOST', '" . $_SESSION["host"] . "' );";

$_SESSION['keyTemplateDel'] = "define( 'AUTH_KEY',         'mettez une phrase unique ici' );";

$_SESSION['keyTemplateTo'] = file_get_contents("https://api.wordpress.org/secret-key/1.1/salt/");

$_SESSION['prefix_from'] = 'wp_';
$_SESSION['prefix_to'] = $_GET["dbprefix"];

$_SESSION['txtDebug_from'] = "define( 'WP_DEBUG', false );";
$_SESSION['txtDebug_to'] = "define( 'WP_DEBUG', true );
define('FS_METHOD','direct');";
/*********************************************************************/

/**
 * Permet de slug une chaine de caractère
 * @param $text
 * @param string $divider
 * @return string
 */
function slugify($text, string $divider = '')
{
    // replace non letter or digits by divider
    $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, $divider);

    // remove duplicate divider
    $text = preg_replace('~-+~', $divider, $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

echo "true";