<?php
session_start();

/**** Base de donnée ****/
$dbname = $_SESSION['dbname'];
$dbuser = $_SESSION['dbuser'];
$dbpass = $_SESSION['dbpdw'];
$dbhost = $_SESSION['host'];

try {
    /**** Se connecte à mysql ****/
    $database = new PDO ("mysql:host=$dbhost", $dbuser, $dbpass);

    /**** Créer la base de donnée ****/
    $database->exec("CREATE DATABASE IF NOT EXISTS `$dbname`;
                CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpass';
                GRANT ALL ON `$dbname`.* TO '$dbuser'@'localhost';
                FLUSH PRIVILEGES;")
    or die(print_r($database->errorInfo(), true));

    /**** Vérifie que la base de donnée fonctionne ****/
    try {
        $db = new PDO('mysql:host=' . $dbhost . ';dbname=' . $dbname, $dbuser, $dbpass);
    } catch (Exception $e) {
        header('HTTP/1.1 500 Internal Server');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'Connexion impossible à la base de donnée', 'code' => 1, 'error' => $e)));
    }

} catch (PDOException $e) {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Erreur lors de la création de la base de donnée', 'code' => 1, 'error' => $e->getMessage())));
}

if (file_exists('../' . $_SESSION['directory'] . 'wp-config.php')) {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Wordpress est déjà installé à cet emplacement', 'code' => 1)));
}

echo "true";