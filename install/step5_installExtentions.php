<?php
session_start();

/**
 * Permet de faire un unzip
 * @param $location
 * @param $new_location
 */
function unzip($location, $new_location)
{
    $zip = new ZipArchive;

// Zip File Name
    if ($zip->open($location) === TRUE) {

        // Unzip Path
        $zip->extractTo($new_location);
        $zip->close();
    } else {
        echo 'Unzipped Process failed';
    }
}

/*** Pointe sur le projet généré ***/
chdir('../../' . $_SESSION['directory']);

/*** Wordpress fonctions ***/
require_once('wp-load.php');
require_once('wp-admin/includes/upgrade.php');
require_once('wp-includes/wp-db.php');

/*** Pointe sur le dossier plugins ***/
chdir('wp-content/plugins');

/*** ACF PRO ***/
exec("git clone https://github.com/wp-premium/advanced-custom-fields-pro.git");

/*** IMAGIFY ***/
exec("curl -O https://downloads.wordpress.org/plugin/imagify.zip");
unzip("imagify.zip", "./");
unlink("imagify.zip");

/*** Infocob CRM form ***/
exec("curl -O https://downloads.wordpress.org/plugin/infocob-crm-forms.1.9.7.zip");
unzip("infocob-crm-forms.1.9.7.zip", "./");
unlink("infocob-crm-forms.1.9.7.zip");

/*** Yoast SEO ***/
exec("curl -O https://downloads.wordpress.org/plugin/wordpress-seo.17.5.zip");
unzip("wordpress-seo.17.5.zip", "./");
unlink("wordpress-seo.17.5.zip");

/*** Active tous les plugins ***/
activate_plugins(array_keys(get_plugins()));

/*** Tests ***/
if (file_exists("./advanced-custom-fields-pro/acf.php") && file_exists("./imagify/imagify.php") && file_exists("./infocob-crm-forms/infocob-crm-forms.php") && file_exists("./wordpress-seo/wp-seo.php")) {
    echo "true";
} else {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => 'Une extention introuvable à cet emplacement : ' . getcwd(), 'code' => 1)));
}