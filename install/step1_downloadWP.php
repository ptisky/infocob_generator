<?php
session_start();

/*** Pointe sur le root du ftp ***/
chdir('../../');

/*** Télécharge Wordpress ***/
shell_exec('curl -O https://fr.wordpress.org/latest-fr_FR.tar.gz');

/*** Unzip Wordpress ***/
shell_exec('tar -zxvf latest-fr_FR.tar.gz');

/*** Rename le fichier par le nom du projet ***/
rename("wordpress", $_SESSION['directory']);

/*** Supprime le ZIP ***/
unlink("latest-fr_FR.tar.gz");

/*** Pointe sur le projet ***/
chdir($_SESSION['directory']);

/*** Créer le dossier upload ***/
mkdir("wp-content/uploads", 0777);

/*** Tests ***/
if (file_exists("./wp-config-sample.php")) {
    echo "true";
} else {
    header('HTTP/1.1 500 Internal Server');
    header('Content-Type: application/json; charset=UTF-8');
    session_destroy();
    die(json_encode(array('message' => 'Le dossier est introuvable à cet emplacement : ' . getcwd(), 'code' => 1)));
}